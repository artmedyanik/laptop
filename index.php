<?php
include("lib/ccomplex.php");

$arr = array(
    ["5+3i", "-5.12"], 
    ["-0.4i", "12-99i"], 
    ["-0.12-0.4i", "67.45-9.5i"], 
    ["i", "6"], 
    ["-i", "12-i"], 
    ["-7i", "12+i"],
);

foreach ($arr as $values)
{
    $obj = new CComplexNumberActions($values[0], $values[1]);
    $val = $obj->sum();
    echo "(".$values[0].") + (".$values[1].") ";
    if ($val !== false) echo " = ".$val;
    else                echo " - ошибки: " . implode("; ", $obj->getErrors());
    echo "<br>";
    
    $val = $obj->diff();
    echo "(".$values[0].") - (".$values[1].") ";
    if ($val !== false) echo " = ".$val;
    else                echo " - ошибки: " . implode("; ", $obj->getErrors());
    echo "<br>";

    $val = $obj->multiplication();
    echo "(".$values[0].") * (".$values[1].") ";
    if ($val !== false) echo " = ".$val;
    else                echo " - ошибки: " . implode("; ", $obj->getErrors());
    echo "<br>";
    
    $val = $obj->division();
    echo "(".$values[0].") * (".$values[1].") ";
    if ($val !== false) echo " = ".$val;
    else                echo " - ошибки: " . implode("; ", $obj->getErrors());
    echo "<br>";
    
    echo "<br>";
}
?>