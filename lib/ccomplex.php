<?php
class CComplexNumberActions
{
    // a+bi - первое число
    // c+di - второе
    
    protected 
        $a, $b, $c, $d,
        $parse_errors = [];
    
    private function parse_number($number)
    {
        $x = $y = 0;
        
        $number = str_replace(" ", "", $number);
        
        $pattern_num = '([+-]?)((\d*)?[.,])?(\d+)?';
        $pattern = '/^'.$pattern_num.$pattern_num.'[*]?[i]\s*$/';
        preg_match($pattern, $number, $matches);

        if (!count($matches))
        {
           if ($number == floatval($number))
                $x = $number;
            else
                $this->parse_errors[] = "$number - ошибочное число";
        }
        elseif (count($matches)==6) // только второе число, либо второе число 1 с + или -
        {
            if     ($number == "i")  $y = 1;
            elseif ($number == "-i") $y = -1;
            else
            {
                if ($matches[5]!="")
                {
                    $x = $matches[1].$matches[2].$matches[4];
                    $y = floatval($matches[5]."1");
                }
                else
                {
                    $y = $matches[1].$matches[2].$matches[4];
                }
            }
        }
        else
        {
            $x = $matches[1].$matches[2].$matches[4];
            $y = $matches[5].$matches[6].$matches[8];
        }
        return array(floatval($x), floatval($y));
    }
    
    public function __construct($first, $second)
    {
        list($this->a, $this->b, $error1) = $this->parse_number($first);
        list($this->c, $this->d, $error2) = $this->parse_number($second);
    }

    private function write_sum($v1, $v2)
    {
        return (($v1!=0) ? $v1 . (($v2>0) ? "+" : "") : "") . (($v2!=0) ? $v2."i" : "");
    }
         
    public function sum()
    {
        $val = false;
        if (empty($this->parse_errors))
        {
            $v1 = $this->a + $this->c;
            $v2 = $this->b + $this->d;
            $val = $this->write_sum($v1, $v2);
        }
        return $val;
    }
    
    public function diff()
    {
        $val = false;
        if (empty($this->parse_errors))
        {
            $v1 = $this->a - $this->c;
            $v2 = $this->b - $this->d;
            $val = $this->write_sum($v1, $v2);
        }
        return $val;
    }
    
    public function multiplication()
    {
        $val = false;
        if (empty($this->parse_errors))
        {
            $v1 = ($this->a * $this->c) - ($this->b * $this->d);
            $v2 = ($this->b * $this->c) + ($this->a * $this->d);
            $val = $this->write_sum($v1, $v2);
        }
        return $val;
    }
    
    public function division()
    {
        $val = false;
        if (empty($this->parse_errors))
        {
            $v1 = ($this->a * $this->c + $this->b * $this->d) / (pow($this->c, 2) + pow($this->d, 2));
            $v2 = ($this->b * $this->c - $this->a * $this->d) / (pow($this->c, 2) + pow($this->d, 2));
            $val = $this->write_sum($v1, $v2);
        }
        return $val;
    }
    
    public function getErrors()
    {
        return $this->parse_errors;
    }

}
?>